FROM denoland/deno:1.14.2

EXPOSE 8080
WORKDIR /app
USER deno

COPY . .
RUN deno cache src/index.ts

CMD ["run", "--allow-net", "--allow-read", "--allow-env", "src/index.ts"]
