export const urlEncode = (data: { [key: string]: string | number | boolean }) =>
  Object.keys(data)
    .map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&');

export type GoogleDriveConfig = {
  refreshToken: string;
  defaultRootId: string;
  clientId: string;
  clientSecret: string;
};

class GoogleDrive {
  config: GoogleDriveConfig;
  accessToken: string;
  expires: number;
  cache: Map<string, { value: string; expires: number }>;
  skipCache: boolean;

  constructor(config: GoogleDriveConfig, skipCache: boolean = false) {
    this.config = config;
    this.accessToken = '';
    this.expires = 0;
    this.cache = new Map();
    this.skipCache = skipCache;
  }

  async initialize() {
    // No need to request a token if it hasn't expired yet
    if (Date.now() < this.expires) return;

    // Check from KV cache
    const tokenCache = this.cacheGet('gd:access-token');
    if (tokenCache) {
      const { accessToken, expires } = JSON.parse(tokenCache);
      if (Date.now() < expires && !!accessToken) {
        // Cached token is still valid, use it
        this.accessToken = accessToken;
        this.expires = expires;
        return;
      }
    }

    const body = urlEncode({
      client_id: this.config.clientId,
      client_secret: this.config.clientSecret,
      refresh_token: this.config.refreshToken,
      grant_type: 'refresh_token',
    });

    const tokenResp = await fetch(
      'https://www.googleapis.com/oauth2/v4/token',
      {
        method: 'post',
        body,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }
    ).then((res) => res.json());

    // Access tokens normally expire after 3600 seconds
    this.expires = Date.now() + 3500000;
    this.accessToken = tokenResp.access_token;

    // Cache it
    this.cachePut(
      'gd:access-token',
      JSON.stringify({
        accessToken: this.accessToken,
        expires: this.expires,
      }),
      3500
    );
  }

  /**
   * Cache utilities
   */
  cacheGet(key: string): string | null {
    const { value, expires } = this.cache.get(key) || { value: '', expires: 0 };
    if (Date.now() > expires) {
      this.cache.delete(key);
      return null;
    }
    return value;
  }

  cachePut(key: string, value: string, expirationTtl = 86400): void {
    const expires = Date.now() + 1000 * expirationTtl;
    this.cache.set(key, { value, expires });
  }

  // Calls the Google Drive v3 API with the appropriate authorization
  async query(path: string, query?: any, headers?: any): Promise<Response> {
    await this.initialize();
    const qs = urlEncode(query);
    return fetch(`https://www.googleapis.com/drive/v3/${path}?${qs}`, {
      headers: {
        Authorization: `Bearer ${this.accessToken}`,
        ...headers,
      },
    });
  }

  // Convert a file path to a file or folder ID
  async getIdFromPath(
    path: string,
    root: string = this.config.defaultRootId,
    skipCache: boolean = this.skipCache
  ) {
    const parts = path.split('/').filter(Boolean);
    const pathClean = parts.join('/');
    const cacheKey = 'gd:path-id:' + root + ':' + pathClean;
    if (!skipCache) {
      const cachedId = this.cacheGet(cacheKey);
      if (cachedId) return cachedId;
    }

    let id: string | null = root;
    for (const part of parts) {
      const name = part.replace(/\'/g, `\\'`);
      const queryResponse: any = await this.query('files', {
        includeItemsFromAllDrives: true,
        supportsAllDrives: true,
        q: `'${id}' in parents and name = '${name}' and trashed = false`,
        fields: 'files(id)',
      })
        .then((res) => res.json())
        .catch((e) => ({ files: [], error: e.response }));

      id = queryResponse.files.length === 0 ? null : queryResponse.files[0].id;
      if (!id) break;
    }

    // Cache ID
    if (id) this.cachePut(cacheKey, id);

    return id;
  }

  async download(id: string, range = '') {
    return await this.query(
      'files/' + id,
      {
        includeItemsFromAllDrives: true,
        supportsAllDrives: true,
        alt: 'media',
      },
      {
        Range: range,
      }
    );
  }

  async downloadByPath(path: string, range = '', root?: string) {
    let id = await this.getIdFromPath(path, root);
    if (!id) return null;
    let download = await this.download(id, range);
    if (download.status === 404) {
      id = await this.getIdFromPath(path, root, true);
      if (!id) return null;
      download = await this.download(id, range);
    }
    return download;
  }

  // Get the first 1000 items in a folder
  async listFolder(id: string) {
    const resp = await this.query('files', {
      includeItemsFromAllDrives: true,
      supportsAllDrives: true,
      q: `'${id}' in parents and trashed = false`,
      orderBy: 'folder,name,modifiedTime desc',
      fields: 'files(name,size)',
      pageSize: 1000,
    }).then((res) => res.json());

    return resp.files.map((file: { name: string; size: string }) => ({
      name: file.name,
      size: Number(file.size),
    }));
  }

  async listFolderByPath(path: string) {
    const id = await this.getIdFromPath(path);
    if (!id) return null;
    return this.listFolder(id);
  }
}

export default GoogleDrive;
