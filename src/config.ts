import 'https://deno.land/x/dotenv/load.ts';

const envMust = (key: string) => {
  const value = Deno.env.get(key);
  if (typeof value === 'undefined')
    throw new Error(`Environment variable ${key} must be set.`);
  return value;
};

const config = {
  listenAddress: Deno.env.get('LISTEN_ADDRESS') || ':8080',
  googleDrive: {
    refreshToken: envMust('GD_REFRESH_TOKEN'),
    defaultRootId: envMust('GD_DEFAULT_ROOT_ID'),
    clientId: envMust('GD_CLIENT_ID'),
    clientSecret: envMust('GD_CLIENT_SECRET'),
  },
};

export default config;
