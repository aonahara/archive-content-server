import { serve } from 'https://deno.land/std@0.189.0/http/server.ts';
import GoogleDrive from './google-drive.ts';
import config from './config.ts';
import { logRequest } from './logger.ts';

const drive = new GoogleDrive(config.googleDrive);

const handler = async (request: Request): Promise<Response> => {
  // Parse URL
  const url = new URL(request.url);
  let path = url.pathname;

  if (path === '/_health')
    return new Response('ok', {
      status: 200,
      headers: { 'Content-Type': 'text/plain' },
    });

  let defaultRootId = config.googleDrive.defaultRootId;
  if (path.startsWith('/gd:')) {
    let rest: string[];
    [defaultRootId, ...rest] = path.split('/').filter(Boolean);
    defaultRootId = defaultRootId.substr(3);
    path = '/' + rest.join('/');
  }
  const fileName = path.split('/').pop() || '';

  // Disallow folder listing
  if (path.endsWith('/')) return new Response('Unauthorized', { status: 403 });

  // Fetch file
  const fileResponse = await drive.downloadByPath(
    path,
    request.headers.get('Range') || '',
    defaultRootId
  );

  if (!fileResponse) return new Response('Not found', { status: 404 });

  // Modify headers
  const headers = new Headers(fileResponse.headers);
  headers.delete('X-GUploader-UploadID');
  headers.delete('X-Goog-Hash');
  headers.delete('X-Amz-Request-Id');
  headers.delete('X-Amz-Expiration');
  headers.delete('X-HW');
  headers.set(
    'Content-Disposition',
    `inline; filename="${encodeURIComponent(fileName || '')}"`
  );
  headers.set('Access-Control-Allow-Origin', '*');
  headers.set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS');
  headers.set('Access-Control-Allow-Headers', 'Range');
  if (fileName.endsWith('.vtt')) headers.set('Content-Type', 'text/vtt');
  if (fileResponse.status >= 200 && fileResponse.status < 300)
    headers.set('Cache-Control', 'public, max-age=604800, immutable');

  return new Response(fileResponse.body, {
    status: fileResponse.status,
    headers,
  });
};

console.log('HTTP webserver running. Access it at', config.listenAddress);
const [hostname, port] = config.listenAddress.split(':');

await serve((request) => logRequest(request, handler(request)), {
  hostname,
  port: parseInt(port),
});
