export async function logRequest(
  request: Request,
  _response: Promise<Response>
) {
  try {
    const t1 = Date.now();
    const response = await _response;
    const originTimeMs = Date.now() - t1;

    console.log(
      `${request.method} ${request.url} | ${response.status} ${originTimeMs}ms`
    );

    return response;
  } catch (ex) {
    const reqId = [Date.now(), Math.random()]
      .map((x) => x.toString(36))
      .join('');
    console.error('Panic!', reqId, ex);
    return new Response('Internal server error. Request ID: ' + reqId, {
      status: 500,
    });
  }
}
