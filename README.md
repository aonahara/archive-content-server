# archive-content-server

Webserver to serve content for [Ragtag Archive](https://archive.ragtag.moe) using Google Drive as the primary storage backend, and nginx for caching.

This project uses [Deno](https://deno.land) instead of Node.js. Run with:

```
deno run --allow-net --allow-env --allow-read src/index.ts
```

Environment variables:

```
GD_DEFAULT_ROOT_ID=
GD_CLIENT_ID=
GD_CLIENT_SECRET=
GD_REFRESH_TOKEN=
```

`GD_DEFAULT_ROOT_ID` is the default folder ID for files which do not specify one.
